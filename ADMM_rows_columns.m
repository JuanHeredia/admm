%%% ADMM DIVISION BY ROWS AND COLUMNS %%%

function [X_Solution error_norm2  primal_residuo2 dual_residuo2] = ADMM_rows_columns(A,y,M,N,rho,lambda,mu,alpha,MAX_ITER)

% A: big matrix of the problem of size m x n
% y: known vector of size m
% M: number of divisions by rows
% N: number of divisions by columns
% rho: augmented parameter
% lambda: norm-1 regularization parameter
% mu: norm-2 regularization parameter
% alpha: proportion between norm-1 and norm-2 regularization [0,1] (0 -->
% norm-1, 1 --> norm-2)

X_Solution = [];    %This will be the final solution

% MAX_ITER = 50;

[m,n] = size(A);
X = zeros(n/N,M+1,N);   % Create a initial vector 0. The first block 
                    % (in the 3rd dimension) is the consensus (z) and it 
                    % is the final solution.
                    % In the third dimension it is stored each subdivision
                    % of the vector x
X_old = X;  % We keep in X_old the values of the previous iteration
U = zeros(n/N,M+1,N);   % U has the same structure as X, but the first block
                    % (in the 3rd dimension) is going to be always zero.
                    
X_mean = zeros(n/N,1,N);  % Vector of mean of replications for each sudivision for x
U_mean = zeros(n/N,1,N);  % Vector of mean of replications for each sudivision for dual variable u
                    
%%% Divisions control
if mod(m/M,1)~=0
    error(['The number of rows is ' num2str(m) '. It is not possible to divide then by ' num2str(M)]);
end
if mod(n/N,1)~=0
    error(['The number of columns is ' num2str(n) '. It is not possible to divide then by ' num2str(N)]);        
end

small_size = [m/M,n/N]; % Size of the small matrices

tic
%%% Inversions
for i=1:M
    for j=1:N
        small_A{i,j} = A((i-1)*m/M+1:i*m/M,(j-1)*n/N+1:j*n/N);
        inverse_matrix{i,j}=inv(eye(m/M)+1/rho*(small_A{i,j}*small_A{i,j}'));
    end    
    small_y{i} = y((i-1)*m/M+1:i*m/M);
end
time_inversion = toc;
disp(['Inverting ' num2str(M*N) ' matrices in ' num2str(time_inversion) ' s']);


tic
%%% Iteration and optimization loop
for k=1:MAX_ITER        % Loop for iterations
    for q=1:N           % Loop for divisions of x
        for p=1:M       % Loop for replications
            y_prime_aux = zeros(small_size(1),1);
            for j=1:N   % Loop for creating y_prime     
                if j~=q
                    y_prime_aux = y_prime_aux + small_A{p,j}*squeeze(X_old(:,p+1,j));
                end                
            end
            y_prime = small_y{p}-y_prime_aux; % definition of y_prime
            First_term = 1/rho*small_A{p,q}'*y_prime;   % 1/rho*A*y'
            Second_term = X(:,1,q)-U(:,p+1,q);   % x-u
            Third_term = 1/rho^2*small_A{p,q}'*(inverse_matrix{p,q}*((small_A{p,q}*small_A{p,q}')*y_prime));    % 1/rho2A*inv*AA*y'
            Fourth_term = 1/rho*small_A{p,q}'*(inverse_matrix{p,q}*(small_A{p,q}*Second_term));   % 1/rhoA*inv*A(z-u)
            % Optimization of x
            X(:,p+1,q) = First_term + Second_term - Third_term - Fourth_term;
        end
    end
    X_mean = mean(X(:,2:end,:),2);  % Compute mean of X
    U_mean = mean(U(:,2:end,:),2);  % Compute mean of U
    
    Z_old = X(:,1,:);
    for q=1:N       % Loop for divisions of x
        % Optimization of z (stored in the first block of X) is done is a
        % loop for each subdivision
        X(:,1,q) = 1/(1+(alpha*mu)/(M*rho))*(soft_thresholding(X_mean(:,:,q)+U_mean(:,:,q),(1-alpha)*lambda/(M*rho)));
    end
    
    X_old = X;  % Let's keep the latest optimization of the X variable
    for q=1:N
        for p=1:M
            % Optimization of U
            U(:,p+1,q) = U(:,p+1,q) + X(:,p+1,q) - X(:,1,q);
        end
    end   
  
    X_Solution = [];
    for j=1:N
        X_Solution = [X_Solution; X(:,1,j)];
    end
    
  error_norm2(k)=norm((A*X_Solution-y),2);         % Calculation of the norm 2 of the error
  primal_res2 = 0;
  dual_res2 = 0;
  for i=1:M
      for j=1:N
          primal_res2 = primal_res2+norm(squeeze(X(:,i+1,j)-X(:,1,j)),2)^2;
      end
  end  
  for j=1:N
      dual_res2 = dual_res2 + norm(X(:,1,j)-Z_old(:,1,j),2)^2;
  end
  primal_residuo2(k)=primal_res2;
  dual_residuo2(k)=M*rho^2*dual_res2;
end
time_optimization = toc;
disp(['Time for optimization loop (' num2str(MAX_ITER) ' iterations): ' num2str(time_optimization) ' s']);
   
